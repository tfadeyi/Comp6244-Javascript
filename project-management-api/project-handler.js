import * as project from "./project-business-logic/project-helper"

//All CRUD operations on Projects
//updates can consist on : adding/removing users to project


//INSERT PROJECT
export async function insert(event, context){
  const requestBody = JSON.parse(event.body);
  return project.insert(requestBody,event)
}

//UPDATE PROJECT
export async function update(event, context){
  const requestBody = JSON.parse(event.body);
  return project.update(requestBody,event)
}

//GET PROJECT BY ID
export async function get(event, context) {
  return project.get(event.pathParameters.id)
}

//DELETE
export async function userDelete(event, context) {
  return project.delete_(event.pathParameters.id)
}

//QUERY ALL PROJECTS
export async function queryAllProjects(event, context) {
  return project.queryAll()
}

//QUERY USER'S PROJECTS
export async function queryUserProjects(event, context) {
  return project.queryUserProjects(event.requestContext.identity.cognitoIdentityId)
}
