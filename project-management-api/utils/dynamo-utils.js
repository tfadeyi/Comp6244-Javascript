import AWS from "aws-sdk";

var dynamoDB = undefined
//init database
function dynamoInit(){
  AWS.config.update({ region: "eu-west-1" });
  return new AWS.DynamoDB.DocumentClient();
}

//use singleton for db instance
export function getInstance(){
  if (dynamoDB==undefined) {
    dynamoDB = dynamoInit()
  }
  return dynamoDB
}

//using promise for async operations
export function call(action, params) {
  return getInstance()[action](params).promise();
}
