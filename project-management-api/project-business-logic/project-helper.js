import * as dynamoDbLib from "../utils/dynamo-utils";
import { success, failure } from "../utils/response-lib";
import uuid from "uuid";

const ProjectTableName = "Projects"

async function isAllowed(userId){
  const params = {
    TableName: "Users",
    Key: {
      id: userId
    }
  };

  try {
    const result = await dynamoDbLib.call("get", params);
    if (result.Item) {
      return result.Item.role === "admin";
    } else {
      return false;
    }
  } catch (e) {
    return false;
  }
}

//validArrayAttribute
export async function insert(requestBody,event){
  var initId = uuid.v1();
  if (requestBody.id!=="") {
    initId = requestBody.id
  }
  var params = {
    TableName: ProjectTableName,
    Item: {
      id: initId,
      creatorId: event.requestContext.identity.cognitoIdentityId,
      name: requestBody.name,
      description: (requestBody.description!=="")?requestBody.description:"",
      status: requestBody.status,
      staff: requestBody.staff,
      projectManager: (requestBody.projectManager!=="")?requestBody.projectManager:null,
      createdAt: Date.now()
    }
  };

  try {
    await dynamoDbLib.call("put", params);
    return success(params.Item);
  } catch (e) {
    return failure({status: false});
  }
}


export async function update(requestBody,event){
  const params = {
    TableName: ProjectTableName,
    Item: {
      id: requestBody.id,
      creatorId: event.requestContext.identity.cognitoIdentityId,
      name: requestBody.name,
      description: (requestBody.description!=="")?requestBody.description:null,
      status: requestBody.status,
      staff: requestBody.staff,
      projectManager: (requestBody.projectManager!=="")?requestBody.projectManager:null,
      createdAt: requestBody.createdAt
    }
  };

  try {
    await dynamoDbLib.call("put", params);
    return success(params.Item);
  } catch (e) {
    console.log(e);
    return failure({status: false});
  }
}

//GET PROJECT BY ID
export async function get(paramId) {
  const params = {
    TableName: ProjectTableName,
    Key: {
      id: paramId
    }
  };

  try {
    const result = await dynamoDbLib.call("get", params);
    if (result.Item) {
      return success(result.Item);
    } else {
      return failure({ status: false, error: "Item not found." });
    }
  } catch (e) {
    return failure({ status: false });
  }
}


//DELETE PROJECT
export async function delete_(paramId) {
  const params = {
    TableName: ProjectTableName,
    Key: {
      id: paramId
    }
  };

  try {
      const result = await dynamoDbLib.call("delete", params);
      return success({ status: true });
    } catch (e) {
      return failure({ status: false });
    }
}

//QUERY ALL PROJECTS
export async function queryAll() {
  const params = {
    TableName: ProjectTableName
  };

  try {
    const result = await dynamoDbLib.call("scan", params);
    // Return the matching list of items in response body
    return success(result.Items);
  } catch (e) {
    return failure({ status: false });
  }
}

//QUERY USER'S PROJECTS
export async function queryUserProjects(paramId) {
  const params = {
    TableName: ProjectTableName,
    IndexName: 'creatorId-index',
    KeyConditionExpression: "creatorId = :creatorId",
    ExpressionAttributeValues: {
      ":creatorId": paramId
    }
  };

  try {
    const result = await dynamoDbLib.call("query", params);
    // Return the matching list of items in response body
    return success(result.Items);
  } catch (e) {
      console.log(e.message);
    return failure({ status: false });
  }
}
