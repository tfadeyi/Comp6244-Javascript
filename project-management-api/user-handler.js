import * as user from "./user-business-logic/user-helper"

//All CRUD operations on users

//UPSERT
export async function upsert(event, context){
  const requestBody = JSON.parse(event.body);
  return user.upsert(requestBody,event.requestContext.identity.cognitoIdentityId)
}

//GET
export async function get(event, context) {
  return user.get(event.pathParameters.id);
}

//DELETE
export async function userDelete(event, context) {
  return user.delete_(event.pathParameters.id);
}

//There's a need for get all users
//QUERY ALL Users
export async function queryUsers(event, context) {
  return user.queryUsers();
}

//GET current user information (Used after user logs in)
export async function getCurrentUser(event, context) {
  return user.getCurrentUser(event.requestContext.identity.cognitoIdentityId)
}
