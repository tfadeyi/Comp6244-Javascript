import * as handler from '../user-handler';

//UPSERT TEST
test('user upsert', async () => {
  const event =
  {
    "body": "{\"name\":\"oluwole\"}",
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'
  const result = await handler.upsert(event, context);
  expect(result.statusCode).toEqual(200);
});

//GET TEST
test('user get', async () => {
  const event =
  {
    "pathParameters": {
      "id": "USER-SUB-1234"
    },
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'
  const expBody = {
    id:"USER-SUB-1234",
    name:"oluwole"
  }

  const result = await handler.get(event, context);
  expect(result.statusCode).toEqual(200);
  expect(JSON.parse(result.body).id).toEqual(expBody.id);
  expect(JSON.parse(result.body).name).toEqual(expBody.name);
});

//Current user GET TEST
test('current user get', async () => {
  const event =
  {
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'
  const expBody = {
    id:"USER-SUB-1234",
    name:"oluwole"
  }

  const result = await handler.getCurrentUser(event, context);
  expect(result.statusCode).toEqual(200);
  expect(JSON.parse(result.body).id).toEqual(expBody.id);
  expect(JSON.parse(result.body).name).toEqual(expBody.name);
});


//QUERY USERS TEST
test('query users', async () => {
  const event =
  {
    "pathParameters": {
      "id": "USER-SUB-1234"
    },
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'
  const expBody = [{
    id:"USER-SUB-1234",
    name:"oluwole"
  }]

  const result = await handler.queryUsers(event, context);
  expect(result.statusCode).toEqual(200);
  var actBody = JSON.parse(result.body);
  var flag = false
  for (var i = 0; i < actBody.length; i++) {
    if (actBody[i]!==undefined) {
      if (actBody[i].id===expBody[0].id) {
        flag = true;
        break;
      }
    }
  }
  expect(flag).toEqual(true);
});


//DELETE TEST
test('user delete', async () => {
  const event =
  {
    "pathParameters": {
      "id": "USER-SUB-1234"
    },
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'

  const result = await handler.userDelete(event, context);
  expect(result.statusCode).toEqual(200);
});
