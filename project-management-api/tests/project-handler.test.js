import * as handler from '../project-handler';

//INSERT TEST
test('project insert', async () => {
  const event =
  {
    "body": "{\"id\":\"PROjeCT1\",\"name\":\"project#1\",\"description\":\"project description.\",\"status\":\"active\",\"staff\":[\"USER-SUB-1234\"]}",
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'
  const result = await handler.insert(event, context);
  expect(result.statusCode).toEqual(200);
});

//UPDATE TEST
test('project update', async () => {
  const event =
  {
    "body": "{\"id\":\"PROjeCT1\",\"name\":\"project#1\",\"description\":\"project description.\",\"status\":\"pending\",\"staff\":\"active\"}",
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'
  const result = await handler.update(event, context);
  expect(result.statusCode).toEqual(200);
});

//GET TEST
test('project get', async () => {
  const event =
  {
    "pathParameters": {
      "id": "PROjeCT1"
    },
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'
  const expBody = {
    id:"PROjeCT1",
    name:"project#1"
  }

  const result = await handler.get(event, context);
  expect(result.statusCode).toEqual(200);
  expect(JSON.parse(result.body).id).toEqual(expBody.id);
  expect(JSON.parse(result.body).name).toEqual(expBody.name);
});

//QUERY PROJECT TEST
test('query project', async () => {
  const event =
  {
    "pathParameters": {
      "id": "PROjeCT1"
    },
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'
  const expBody = [{
    id:"PROjeCT1"
  }]

  const result = await handler.queryAllProjects(event, context);
  expect(result.statusCode).toEqual(200);
  var actBody = JSON.parse(result.body);
  var flag = false
  for (var i = 0; i < actBody.length; i++) {
    if (actBody[i].id===expBody[0].id) {
      flag = true;
      break;
    }
  }
  expect(flag).toEqual(true);
});

//QUERY PROJECT TEST
test('query project by user', async () => {
  const event =
  {
    "pathParameters": {
      "id": "USER-SUB-1234"
    },
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'
  const expBody = [{
    id:"PROjeCT1",
    creatorId:"USER-SUB-1234"
  }]

  const result = await handler.queryUserProjects(event, context);
  expect(result.statusCode).toEqual(200);
  var actBody = JSON.parse(result.body);
  var flag = false
  for (var i = 0; i < actBody.length; i++) {
    if (actBody[i].id===expBody[0].id) {
      flag = true;
      break;
    }
  }
  expect(flag).toEqual(true);
});


//DELETE TEST
test('project delete', async () => {
  const event =
  {
    "pathParameters": {
      "id": "PROjeCT1"
    },
    "requestContext": {
      "identity": {
        "cognitoIdentityId": "USER-SUB-1234"
      }
    }
  };
  const context = 'context'

  const result = await handler.userDelete(event, context);
  expect(result.statusCode).toEqual(200);
});
