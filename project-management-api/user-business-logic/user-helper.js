import * as dynamoDbLib from "../utils/dynamo-utils";
import { success, failure } from "../utils/response-lib";

//All CRUD operations on users

export const UserTableName = "Users"

//UPSERT
export async function upsert(requestBody, paramId){
  const params = {
    TableName: UserTableName,
    Item: {
      id: paramId,
      email: requestBody.email,
      name: requestBody.name,
      role: requestBody.role
    }
  };

  try {
    await dynamoDbLib.call("put", params);
    return success(params.Item);
  } catch (e) {
    return failure({ status: false });
  }
}

//GET
export async function get(paramId) {
  const params = {
    TableName: UserTableName,
    Key: {
      id: paramId
    }
  };

  try {
    const result = await dynamoDbLib.call("get", params);
    if (result.Item) {
      return success(result.Item);
    } else {
      return failure({ status: false, error: "Item not found." });
    }
  } catch (e) {
    return failure({ status: false });
  }
}

//DELETE
export async function delete_(paramId) {
  const params = {
    TableName: UserTableName,
    Key: {
      id: paramId
    }
  };

  try {
      const result = await dynamoDbLib.call("delete", params);
      return success({ status: true });
    } catch (e) {
      return failure({ status: false });
    }
}

//There's a need for get all users
//QUERY ALL Users
export async function queryUsers() {
  const params = {
    TableName: UserTableName
  };

  try {
    const result = await dynamoDbLib.call("scan", params);
    // Return the matching list of items in response body
    return success(result.Items);
  } catch (e) {
    return failure({ status: false });
  }
}

//GET current user information (Used after user logs in)
export async function getCurrentUser(paramId) {
  const params = {
    TableName: UserTableName,
    Key: {
      id: paramId
    }
  };

  try {
    const result = await dynamoDbLib.call("get", params);
    if (result.Item) {
      return success(result.Item);
    } else {
      return failure({ status: false, error: "Item not found." });
    }
  } catch (e) {
    return failure({ status: false });
  }
}
