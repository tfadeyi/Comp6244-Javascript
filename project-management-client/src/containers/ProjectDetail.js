import React, { Component, Fragment } from "react";
import { API } from "aws-amplify";
import {
  HelpBlock,
  FormGroup,
  FormControl,
  ControlLabel,
  ListGroup,
  ListGroupItem
} from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import LoaderButton from "../components/LoaderButton";
import config from "../config";
import "./ProjectDetail.css";

export default class ProjectDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      project: null,
      isNew: false,
      isLoading: false,
      name: "",
      description: "",
      status:"",
      staff: [],
      staffMembers: [],
      newDevelopers: "",
      availableDevelopers: [],
      tmpDevelopers: [],
      projectManager:""
    };
  }

  async componentDidMount() {
    this.setState({ isLoading: true });
    var isNewTmp = (this.props.match.params.id==="new")

    this.setState({
      isNew: isNewTmp
    })

    if (isNewTmp) {
      this.setState({ isLoading: false });
      return
    }

    try {
      const project = await this.getProject();
      const availableDevelopers = await this.getDevelopers();
      const { name, description, status, staff ,projectManager} = project;
      //get staff member
      var staffMembers = this.getStaffInfo(staff);

      this.setState({
        project,
        name,
        description,
        status,
        staff,
        staffMembers,
        availableDevelopers,
        projectManager
      });

    } catch (e) {
      alert(e);
    }
    this.setState({ isLoading: false });
  }

  async getStaffInfo(staff){
    var staffMembers = []
    for (var i = 0; i < staff.length; i++) {
      const user = await this.getStaff(staff[i]);
      staffMembers.push(user);
    }
    this.setState({staffMembers})
    return staffMembers;
  }

  getProject() {
    return API.get("project-management-api", `projects/${this.props.match.params.id}`);
  }

  getStaff(id) {
    return API.get("project-management-api", `users/${id}`);
  }

  validateForm() {
    return (this.isValidUser()) &&
            this.state.name.length>0 &&
            this.state.description.length>0;
  }

  saveProject(project,upsert) {
    //upsert is either create or update
    return API.post("project-management-api","projects/"+upsert+"/",{body: project})
  }

  async getDevelopers(staffMembers){
     const users = await API.get("project-management-api", `users/`);
     return users.filter((user,i) =>
       (user.role==="developer")
     )
  }

  isValidUser(){
    return (this.props.globalUser.role==="admin") ||
      (this.props.globalUser.id===this.state.projectManager)
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }


  parseDevelopers(){
    var tmpStaff = this.state.staff

    if (this.state.tmpDevelopers!==[]) {
      const newIds = this.state.tmpDevelopers
      for (var i = 0; i < newIds.length; i++) {
        const id = newIds[i].trim().toLowerCase();
        if(!tmpStaff.includes(id)){
          tmpStaff.push(id)
        }
      }
    }
    return tmpStaff;
  }

  handleNewProjectSubmit = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });
    try {
      var tmpStaff = this.state.tmpDevelopers;
      var project = {
          id: "",
          creatorId: this.props.globalUser.id,
          name: this.state.name,
          description: this.state.description,
          status: "active",
          staff: tmpStaff,
          projectManager: this.state.projectManager
      };

      //need to add the project to the db
      await this.saveProject(project,"create");
      await this.getStaffInfo(tmpStaff);
      //update project state
      this.setState({
        project: project,
        staff: tmpStaff,
        projectManager: project.projectManager
      })
      alert("Data was saved!")
    } catch (e) {
      console.log(e.message);
      alert(e.message);
    }
    this.setState({ isLoading: false });
    this.props.history.push("/");
  }

//When editing project details
  handleSubmit = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });
    try {
      var tmpStaff = this.parseDevelopers();
      var project = {
          id: this.state.project.id,
          creatorId: this.state.project.creatorId,
          name: this.state.name,
          description: this.state.description,
          status: (this.state.status!=="")?this.state.status:this.state.project.status,
          staff:tmpStaff,
          createdAt: this.state.project.createdAt,
          projectManager: (this.state.projectManager!=="")?
          this.state.projectManager:this.state.project.projectManager
      };

      await this.saveProject(project,"update");
      await this.getStaffInfo(tmpStaff);
      //update project state
      this.setState({
        project: project,
        staff: tmpStaff,
        projectManager: project.projectManager
      })
      alert("Data was saved!")
    } catch (e) {
      alert(e.message);
    }
    this.setState({ isLoading: false });
  }

  onPickStatus(e){
    this.setState({ status: this.inputEl.value });
  }

  onPickDeveloper(e){
    console.log(this.developerElement.value);
    var tmpDev = this.state.tmpDevelopers;
    if (tmpDev.includes(this.developerElement.value)) {
      return
    }
    tmpDev = [this.developerElement.value].concat(this.state.tmpDevelopers)
    this.setState({ tmpDevelopers: tmpDev });
    console.log(tmpDev);
  }

  onPickProjectManager(e){
    this.setState({ projectManager: this.managerElement.value });
  }

  renderAdminCommands(){
      //add dev
      //add project manager
      return(
        <Fragment>
          <FormGroup controlId="formControlsSelectMultiple">
            <ControlLabel>Select Developers</ControlLabel>
            <FormControl componentClass="select" multiple
            value={this.state.tmpDevelopers}
            inputRef={ el => this.developerElement=el }
            onChange={this.onPickDeveloper.bind(this)}>
              {[].concat(this.state.availableDevelopers).map( (developer,i) =>
                <option value={developer.id}> {developer.name}</option>
              )}
            </FormControl>
          </FormGroup>
          {
            (this.props.globalUser.role==="admin")
            ? <FormGroup controlId="formControlSelect" bsSize="large">
              <ControlLabel>Select Project Manager</ControlLabel>
                <FormControl
                      onChange={this.onPickProjectManager.bind(this)}
                      inputRef={ el => this.managerElement=el }
                      componentClass="select" placeholder="select">
                      {[].concat(this.state.availableDevelopers).map( (developer,i) =>
                        <option value={developer.id}> {developer.name}</option>
                      )}
                  </FormControl>
            </FormGroup>
            :null
          }
        </Fragment>
      );
  }

  renderStaffList(staffMembers1){
    if (staffMembers1===undefined){
      return
    }
    const staffMembers = staffMembers1

    return <ListGroup>{[{}].concat(staffMembers).map( (member,i) =>
        i !== 0
        ?<ListGroupItem key={member.id}>
              {(member.id===this.state.project.projectManager)? member.name+" is Manager":member.name}
          </ListGroupItem>
        :<ListGroupItem key="nothing"  header="Project Members">
          </ListGroupItem>
    )}</ListGroup>
  }

  renderNewProjectForm() {
    if (this.props.globalUser.role!=="admin") {
      this.props.history.push("/");
      return
    }
    return (
      <form onSubmit={this.handleNewProjectSubmit}>
        <FormGroup controlId="name" bsSize="large">
          <ControlLabel>Name</ControlLabel>
          <FormControl
            autoFocus
            type="text"
            value={this.state.name}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup controlId="description" bsSize="large">
          <ControlLabel>Description</ControlLabel>
          <FormControl
            value={this.state.description}
            onChange={this.handleChange}
            componentClass="textarea"
          />
        </FormGroup>
        <FormGroup controlId="status" bsSize="large">
          <ControlLabel>Status</ControlLabel>
          <FormControl
            readOnly
            value="active"
            type="text"
          />
        </FormGroup>
        {(this.isValidUser())? this.renderAdminCommands():null}
        <Fragment>
          {!this.state.isLoading && this.renderStaffList(this.state.staffMembers)}
        </Fragment>
        <LoaderButton
          block
          bsSize="large"
          disabled={!this.validateForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Create New Project"
          loadingText="Saving Project…"
        />
      </form>
    );
  }

  renderForm() {
    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup controlId="name" bsSize="large">
          <ControlLabel>Name</ControlLabel>
          {!(this.isValidUser())
          ?<FormControl
            readOnly
            autoFocus
            type="text"
            value={this.state.name}
            onChange={this.handleChange}/>
          :<FormControl
            autoFocus
            type="text"
            value={this.state.name}
            onChange={this.handleChange}/>
          }
        </FormGroup>
        <FormGroup controlId="description" bsSize="large">
          <ControlLabel>Description</ControlLabel>
          {!(this.isValidUser())
          ?<FormControl
            readOnly
            value={this.state.description}
            onChange={this.handleChange}
            componentClass="textarea"/>
            :<FormControl
              value={this.state.description}
              onChange={this.handleChange}
              componentClass="textarea"/>
          }
        </FormGroup>
        <FormGroup controlId="status" bsSize="large">
          <ControlLabel>Status: {this.state.status}</ControlLabel>
          {!(this.isValidUser())
          ?null
            :<FormControl
                  onChange={this.onPickStatus.bind(this)}
                  inputRef={ el => this.inputEl=el }
                  componentClass="select" placeholder="select">
                <option value="active">Active</option>
                <option value="pending">Pending</option>
                <option value="completed">Completed</option>
              </FormControl>
          }
        </FormGroup>
        {(this.isValidUser())? this.renderAdminCommands():null}
        <Fragment>
          {!this.state.isLoading && this.renderStaffList(this.state.staffMembers)}
        </Fragment>
        <LoaderButton
          block
          bsSize="large"
          disabled={!this.validateForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Edit Project Details"
          loadingText="Saving Changes…"
        />
      </form>
    );
  }

  render() {
    return (
      <div className="ProjectDetail">
        {this.state.isNew? this.renderNewProjectForm():this.renderForm()}
      </div>
    );
  }

}
/*
<FormGroup controlId="data_origin" bsSize="large">
  <ControlLabel>{"Created: " + new Date(this.state.project.createdAt)}</ControlLabel>
  //<ControlLabel>{"Created by: " + this.state.project.}</ControlLabel>
</FormGroup>
// <ListGroup>
//
// </ListGroup>
*/
