import React, { Component } from "react";
import { PageHeader, ListGroup, ListGroupItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { API } from "aws-amplify";
import "./Home.css";

export default class Home extends Component {
  constructor(props) {
      super(props);

      this.state = {
        isLoading: true,
        projects: []
      };
  }

  async componentDidMount() {
    if (!this.props.isAuthenticated) {
      return;
    }

    try {
      var projectsTmp = await this.getProjects();
      const userRole = this.props.globalUser.role;
      if (userRole!=="admin") {
        projectsTmp = await this.getUserProjects(projectsTmp,this.props.globalUser.id)
      }
      this.setState({ projects:projectsTmp });
    } catch (e) {
      alert(e);
    }

    this.setState({ isLoading: false });
  }

  getProjects() {
    return API.get("project-management-api","projects/");
  }

  getUserProjects(projectsTmp,userId) {
    return projectsTmp.filter((project,i) =>
      (project.staff.includes(userId))
    )
  }

  renderLander() {
      return (
        <div className="lander">
          <h1>Project Management Tool</h1>
          <p>An app to manage your projects</p>
        </div>
      );
  }

  renderProjectList(projects) {
    return [{}].concat(projects).map(
      (project, i) =>
        i !== 0
          ? <LinkContainer
              key={project.id}
              to={`/projects/${project.id}`}
            >
              <ListGroupItem header={project.name.trim().split("\n")[0]}>
                {"Created: " + new Date(project.createdAt).toLocaleString() + " Status: " +project.status}
              </ListGroupItem>
            </LinkContainer>
          : (this.props.globalUser.role==="admin")?<LinkContainer
              key="new"
              to="/projects/new"
            >
              <ListGroupItem>
                <h4>
                  <b>{"\uFF0B"}</b> Create a new project
                </h4>
              </ListGroupItem>
            </LinkContainer> : null
    );
  }

  renderProjects() {
      return (
        <div className="projects">
          <PageHeader>Welcome {this.props.globalUser.name}</PageHeader>
          <ListGroup>
            {!this.state.isLoading && this.renderProjectList(this.state.projects)}
          </ListGroup>
        </div>
      );
  }

  render() {
    return (
      <div className="Home">
        {this.props.isAuthenticated ? this.renderProjects() : this.renderLander()}
      </div>
    );
  }
}
