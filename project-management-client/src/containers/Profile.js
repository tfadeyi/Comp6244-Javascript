import React, { Component } from "react";
import { API, Storage } from "aws-amplify";
import {
  HelpBlock,
  FormGroup,
  FormControl,
  ControlLabel
} from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.file = null;

    this.state = {
      isLoading: false,
      email: "",
      name: "",
      role: ""
    };
  }

  validateForm() {
    return this.state.name.length > 0;
  }

  saveUserToDb(user) {
    return API.post("project-management-api","users/upsert/",{body: user})
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      const user = {
        id: this.props.globalUser.id,
        email: this.props.globalUser.email,
        name: (this.state.name==="")?this.props.globalUser.name:this.state.name,
        role: (this.props.globalUser.role).toLowerCase()
      };
      //need to add the user to the db
      await this.saveUserToDb(user);
      //setting the user state to global
      this.props.setGlobalUser(user);//set global user
      alert("Data was saved!")
    } catch (e) {
      alert(e.message);
    }
    this.setState({ isLoading: false });
  }

  renderForm() {
    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            autoFocus
            readOnly
            type="email"
            value={this.props.globalUser.email}
            onChange={this.handleChange}
            placeholder={this.props.globalUser.email}
          />
        </FormGroup>
        <FormGroup controlId="name" bsSize="large">
          <ControlLabel>Name</ControlLabel>
          <FormControl
            value={this.state.name}
            onChange={this.handleChange}
            type="text"
            placeholder={this.props.globalUser.name}
          />
        </FormGroup>
        <FormGroup controlId="formControlsSelect">
          <ControlLabel>Role: {this.props.globalUser.role}</ControlLabel>
        </FormGroup>
        <LoaderButton
          block
          bsSize="large"
          disabled={!this.validateForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Edit Profile"
          loadingText="Saving Changes…"
        />
      </form>
    );
  }

  render() {
    return (
      <div className="Profile">
        {this.renderForm()}
      </div>
    );
  }

}
