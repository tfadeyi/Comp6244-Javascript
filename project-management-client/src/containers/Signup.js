import React, { Component } from "react";
import {
  HelpBlock,
  FormGroup,
  FormControl,
  ControlLabel
} from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import { Auth,API } from "aws-amplify";
import "./Signup.css";

export default class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: "",
      password: "",
      role: "admin",
      confirmPassword: "",
      confirmationCode: "",
      newUser: null
    };
  }

  validateForm() {
    return (
      this.state.email.length > 0 &&
      this.state.password.length > 0 &&
      this.state.password === this.state.confirmPassword
    );
  }

  validateConfirmationForm() {
    return this.state.confirmationCode.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  // onPickColor(e){
  //     console.log('[onPickColor]', e.target.id )
  //     this.setState({ [e.target.id]: "this.inputEl.value" });
  //     console.log(this.props.role);
  // }

  // handleSelectChange = event => {
  //   this.setState({
  //     [event.target.id]: event.inputEl.value
  //   });
  //   console.log(this.props.role);
  // }

  saveUserToDb(user) {
    return API.post("project-management-api","users/upsert/",{body: user})
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      const newUser = await Auth.signUp({
        username: this.state.email,
        password: this.state.password
      });
      this.setState({
        newUser
      });
    } catch (e) {
      alert(e.message);
    }

    this.setState({ isLoading: false });

  }

  handleConfirmationSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await Auth.confirmSignUp(this.state.email, this.state.confirmationCode);
      await Auth.signIn(this.state.email, this.state.password);

      this.props.userHasAuthenticated(true);
      const user = {
        email: this.state.email,
        role: (this.state.role).toLowerCase()
      };
      //need to add the user to the db
      await this.saveUserToDb(user);
      //setting the user state to global
      this.props.setGlobalUser(user);
      this.props.history.push("/");
    } catch (e) {
      alert(e.message);
      this.setState({ isLoading: false });
    }

  }

  onPickRole(e){
    this.setState({ role: this.inputEl.value });
  }

  renderConfirmationForm() {
    return (
      <form onSubmit={this.handleConfirmationSubmit}>
        <FormGroup controlId="confirmationCode" bsSize="large">
          <ControlLabel>Confirmation Code</ControlLabel>
          <FormControl
            autoFocus
            type="tel"
            value={this.state.confirmationCode}
            onChange={this.handleChange}
          />
          <HelpBlock>Please check your email for the code.</HelpBlock>
        </FormGroup>
        <LoaderButton
          block
          bsSize="large"
          disabled={!this.validateConfirmationForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Verify"
          loadingText="Verifying…"
        />
      </form>
    );
  }

  renderForm() {
    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            autoFocus
            type="email"
            value={this.state.email}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>Password</ControlLabel>
          <FormControl
            value={this.state.password}
            onChange={this.handleChange}
            type="password"
          />
        </FormGroup>
        <FormGroup controlId="formControlsSelect">
          <ControlLabel>Role</ControlLabel>
          <FormControl
              onChange={this.onPickRole.bind(this)}
              inputRef={ el => this.inputEl=el }
              componentClass="select" placeholder="select">
            <option value="admin">Admin</option>
            <option value="developer">Developer</option>
          </FormControl>
        </FormGroup>
        <FormGroup controlId="confirmPassword" bsSize="large">
          <ControlLabel>Confirm Password</ControlLabel>
          <FormControl
            value={this.state.confirmPassword}
            onChange={this.handleChange}
            type="password"
          />
        </FormGroup>
        <LoaderButton
          block
          bsSize="large"
          disabled={!this.validateForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Signup"
          loadingText="Signing up…"
        />
      </form>
    );
  }

  render() {
    return (
      <div className="Signup">
        {this.state.newUser === null
          ? this.renderForm()
          : this.renderConfirmationForm()}
      </div>
    );
  }
}
/*
<FormGroup controlId="role" bsSize="large">
  <ControlLabel>Role</ControlLabel>
  <FormControl componentClass="select" placeholder="select"
    inputRef={ el => this.inputEl=el } onChange={this.onPickColor.bind(this)}>
    <option value="admin">Admin</option>
    <option value="project-manager">Project Manager</option>
    <option value="developer">Developer</option>
  </FormControl>
</FormGroup>
*/
